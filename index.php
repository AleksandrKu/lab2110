<?php
try {  //
require_once "Autoload.php";  //подключаем метод автолоад, которій будет нам подключать все классы
spl_autoload_register("Autoload::load_class"); //автозагрузчик Autoload-класс load_class-статический метод

	$asus = new \app\Asus(); // так как класс Asus в namespace app
	$asus->start();  //
	$asus->printParameters();
	$asus->identifyUser();
	$asus->restart2();
	echo PHP_EOL;

	$lenovo = new \app\Lenovo();
	$lenovo->start();
	$lenovo->printParameters();
	$lenovo->identifyUser();
	echo PHP_EOL;

	$macbook = new \app\MacBook();
	$macbook->start();
	$macbook->printParameters();
	$macbook->identifyUser();
	echo PHP_EOL;

}
catch (\exception\ComputerException $e) {
	\helpers\Console::printLine($e->getMessage(),"WARNING");
}
catch (Exception $ex) {
	echo "Error";
}