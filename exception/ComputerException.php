<?php
namespace exception;  // определяем пространство видимости єтого класса

class ComputerException extends \Exception // наследуем от встроенного в PHP класса Exception
{
	public function __construct($message, $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous); // обращаемся к конструктору  встроенного в PHP класса Exception
	}
}