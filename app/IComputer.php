<?php
namespace app;

interface IComputer  // наследуемые классы должны описывать все эти методы
{
function start(); // все методы должны быть публичными
function shutDown();
function restart2();
function printParameters();
function identifyUser();
}