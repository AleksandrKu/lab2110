<?php
namespace app;
use \helpers\Console as Console;
use exception\ComputerException as ComputerException;
abstract class Computer   // Если хоть один метод австрактный, то и класс обастрактный
{
	private $cpu;
	private $ram;
	private $video;
	private $memory;
	private $isWorking = false; //Boolean variable with computer’s current statement (on/off)
	private $computerName = 'Computer';

	protected function setCpu($cpu1) // так как свойство $cpu private , то создаю ему  методы set и get, чтобы можно было изменять и отдавать это свойство
	{
		$this->cpu = $cpu1;
	}
	public function getCpu()
	{
		return $this->cpu;
	}

	protected function setRam($ram1)
	{
		$this->ram = $ram1;
	}
	public function getRam()
	{
		return $this->ram;
	}

	protected function setVideo($video1)
	{
		$this->video = $video1;
	}
	public function getVideo()
	{
		return $this->video;
	}

	protected function setMemory($memory1)
	{
		$this->memory = $memory1;
	}
	public function getMemory()
	{
		return $this->memory;
	}

	public function setComputerName($computerName1)
	{
		$this->computerName = $computerName1;
	}
	public function getComputerName()
	{
		return $this->computerName;
	}

	public function start()
	{
		$this->isWorking = true;
		Console::printLine("Welcome user!","SUCCESS");
	}

	public function shutdown()
	{
		$this->isWorking = false;
		Console::printLine("Goodbuy!", "SUCCESS");
	}

	public function restart2()
	{
		if ($this->isWorking) {
			$this->shutDown();
			for ($t = 1; $t <= 5; $t++) {
				echo '.';
				sleep(1);
			}
			echo PHP_EOL;
			$this->start();
		} else {
			throw  new ComputerException($this->getComputerName(). "!!!!Computer must be Turn On for restart");
/*			Console::printLine("Computer must be Turn On for restart", "WARNING");*/
		}
	}

	public function printParameters()
	{
		if ($this->isWorking) {
			echo $this->cpu;
			echo $this->ram;
			echo $this->video;
			echo $this->memory;
		} else {
			echo "Error! Computer OFF. Switch ON computer";
		}
	}

	public abstract function identifyUser();
}





