<?php
namespace app;  // область видимости этого класса
use helpers\Console; // подключаем этот класс
class  Asus extends Computer implements IComputer // наследуем класс Computer и интерфейс IComputer
{
	const IS_DESKTOP = false;
	public function __construct()  //  этот метод выполняется при создании объекта класса
	{
		$this->setCpu('Intel Core i3-4005U (1.7 Ghz)'); // передаем параметры компьютеров в через функцию set , так как одноименные свойства имеют модификатор доступа private
		$this->setRam('6 Gb');
		$this->setVideo('nVidia GeForce GT 920M');
		$this->setMemory('HDD 1 Tb');
		$this->setComputerName('Asus X540LJ');
	}
	public function identifyUser()  //
	{
		echo $this->getComputerName() . ': Identify by password' . PHP_EOL;
		Console::printLine($this->getComputerName() . ': Identify by login and password', Console::$note); // обращаемся к статическому методу pintLine класса Console , передаем название компьютера из класса Computer методом getComputerName + сообщение , вторым параметром перадаем статикеское свойство $note из класса Console

	}
}